#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void sort(int base[], int length);
void print_list(int base[], int length);
void test(int base[], int length);
void tests();
void benchmarks();
void swap(); /* created a new subroutine switch both base[j] and base[j+1] */


void swap(int *x, int *y){
  int z = *x;
   *x = *y;
   *y = z;
}
void sort(int base[], int length) {
   int i, j; 
   
   for (i = 0; i < length-1; i++){  
      for (j = 0; j < length-1; j++){
           if (base[j] > base[j+1]){
             swap(&base[j], &base[j+1]); 
         }
      }
    }              
}

/* Print a list to standard output. */
void print_list(int base[], int length) {
    for (int n = 0; n < length; n++) {
        printf("%d ", base[n]);
    }
    printf("\n");
}

/* Print a list, sort it, then print it again. */
void test(int base[], int length) {
    printf("Before sorting: ");
    print_list(base, length);
    sort(base, length);
    printf("After sorting:  ");
    print_list(base, length);
}

/* Run some tests of the sorting algorithm. */
void tests() {
    int test0[] = {0, 1, 5, -4, -8, 3, 2, 9, -7, 6};
    test(test0, sizeof(test0)/sizeof (int));
 
    int test1[] = {};
    test(test1 , sizeof(test1)/sizeof (int));
    int test2[] = {0};
    test(test2 , sizeof(test2)/sizeof (int));
    int test3[] = {1,0};
    test(test3 , sizeof(test3)/sizeof (int));
    int test4[] = {0,1,5,5,3,2,6,7,7,7};
    test(test4 , sizeof(test4)/sizeof (int));

   
}

/* Benchmark the sorting algorithm. */
void benchmarks() {

 int rand(void);
 
 int count = 0; // sets count to 0
 srand(time(0)); // initiatizes rand 
 printf("Calculating...\n");
 printf("Length, Seconds\n");
 // run it ten times 
 while (count != 10){ // stops the while loop after 10 
  int length,j;
    for( j = 0 ; j < count ; j++ ) {
      length = rand() % 100000;
      
      // create a random length between 0 to 30  
  } 
  
  
  int base[length],i;
  for( i = 0 ; i < length ; i++ ) { // stops until length count. 
      base[i] = rand() % 50; // each value in the array has a random value between 0 and 50
   }


  clock_t t;
  t = clock(); // start clock
  sort(base,length); // call sort with a random values 
  t = clock() - t; // stops the clock and finds the difference between the times.

 printf (" %d , %f \n",length,((float)t)/CLOCKS_PER_SEC); // finds it in seconds
  
  count++; // adds 1 to the counter
 }
printf("--------END----------\n"); 
}
int main() {
    tests();
    benchmarks();
    exit(0);
}